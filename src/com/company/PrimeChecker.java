package com.company;

public class PrimeChecker {
    public boolean check(int num){
        if(num==1)
            return false;
        if(num==2)
            return true;
        boolean flag = true;
        for(int i=3;i*i<=num;i+=2) {
            if(num%i==0)
                flag = false;
        }
        if(num%2==0&&num>3)
            flag = false;
        return flag;
    }
}
