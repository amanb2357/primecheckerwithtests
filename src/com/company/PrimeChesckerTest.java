package com.company;

import org.junit.Assert;
import org.junit.Test;

public class PrimeChesckerTest {

    @Test
    public void testCheckNonPrime1(){
        PrimeChecker checker = new PrimeChecker();
        boolean result1 = checker.check(55);
        Assert.assertEquals(false, result1);
    }
    @Test
    public void testCheckNonPrime2(){
        PrimeChecker checker = new PrimeChecker();
        boolean result1 = checker.check(100);
        Assert.assertEquals(false, result1);
    }
    @Test
    public void testCheckNonPrime3(){
        PrimeChecker checker = new PrimeChecker();
        boolean result1 = checker.check(6);
        Assert.assertEquals(false, result1);
    }
    @Test
    public void testCheckPrime1(){
        PrimeChecker checker = new PrimeChecker();
        boolean result1 = checker.check(2);
        Assert.assertEquals(true, result1);
    }
    @Test
    public void testCheckPrime2(){
        PrimeChecker checker = new PrimeChecker();
        boolean result1 = checker.check(13);
        Assert.assertEquals(true, result1);
    }
    @Test
    public void testCheckPrime3(){
        PrimeChecker checker = new PrimeChecker();
        boolean result1 = checker.check(17);
        Assert.assertEquals(true, result1);
    }
    @Test
    public void testCheckPrime4(){
        PrimeChecker checker = new PrimeChecker();
        boolean result1 = checker.check(17);
        Assert.assertEquals(true, result1);
    }
    @Test
    public void testCheckPrime5(){
        PrimeChecker checker = new PrimeChecker();
        boolean result1 = checker.check(11);
        Assert.assertEquals(true, result1);
    }
}
